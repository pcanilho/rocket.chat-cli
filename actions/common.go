package actions

import (
	"fmt"
	"github.com/pkg/errors"
	"gitlab.com/pcanilho/rocket.chat-cli/helpers/formatters"
	"io"
	"os"
)

type FormatterType = string
const (
	Template FormatterType = "template"
	Table                  = "table"
)

func GetFormatter(fType FormatterType, templateGlob string) (formatters.Formatter, error) {
	switch fType {
	case Template:
		return formatters.NewTemplateFormatter(templateGlob)
	case Table:
		return formatters.NewTablePrinter(templateGlob)
	default:
		return nil, fmt.Errorf("the supplied formatter type [%v] is not supported", fType)
	}
}

type OutputType = string
const (
	Stdout OutputType 	= "stdout"
	File				= "file"
)

func GetWriter(oType OutputType, destination string) (io.Writer, error) {
	switch oType {
	case Stdout:
		return os.Stdout, nil
	case File:
		if f, err := os.Create(destination); err != nil {
			return nil, errors.Wrapf(err, "unable to create the destination file [%v]", destination)
		} else {
			return f, nil
		}
	default:
		return nil, fmt.Errorf("the supplied output type [%v] is not supported", oType)
	}
}
