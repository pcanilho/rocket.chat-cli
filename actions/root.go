package actions

import (
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/pcanilho/rocket.chat-cli/api"
)

var (
	version, commit, name string
)

func init() {
	RootCmd.PersistentFlags().StringP("server", "s", "", "The server address to be used when contacting a Rocket.Chat. server [ROCKET_SERVER]")
	RootCmd.PersistentFlags().CountP("verbose", "v", "If provided, debug information will be printed to the stdout.")
	RootCmd.PersistentFlags().StringP("id", "i", "", "The token Id to be used when contacting a Rocket.Chat server. [ROCKET_TOKEN_ID]")
	RootCmd.PersistentFlags().StringP("token", "t", "", "The token to be used when contacting a Rocket.Chat server. If not provided, the user will be prompted for each action. [ROCKET_TOKEN]")
}

var RootCmd = &cobra.Command{
	Use: name,
	Version: fmt.Sprintf("v%s (%s)", version, commit),
	PersistentPreRunE: func(cmd *cobra.Command, args []string) error {
		fmt.Println(version)
		serverAddress, _ := cmd.Flags().GetString("server")
		verbosityLevel, _ := cmd.Flags().GetCount("verbose")
		id, _ := cmd.Flags().GetString("id")
		token, _ := cmd.Flags().GetString("token")

		// Lookup for environment variables
		envId, found := os.LookupEnv("ROCKET_TOKEN_ID")
		if found && len(strings.TrimSpace(envId)) > 0 {
			id = envId
		}
		envToken, found := os.LookupEnv("ROCKET_TOKEN")
		if found && len(strings.TrimSpace(envToken)) > 0 {
			token = envToken
		}
		envAddress, found := os.LookupEnv("ROCKET_SERVER")
		if found && len(strings.TrimSpace(envAddress)) > 0 {
			serverAddress = envAddress
		}

		// Setup logging
		var apiLogger *log.Logger
		var backendLogging bool
		// Enable the API logging
		if verbosityLevel >= 1 {
			apiLogger = log.New(os.Stdout, "[API] :: ", log.LstdFlags)
		}
		// Enable the backend SDK logging
		if verbosityLevel >= 2 {
			backendLogging = true
		}

		// Setup client
		credentials := &api.Credentials{TokenID: id, Token: token}
		_, err := api.GetInstance(serverAddress, credentials,
			api.WithApiLogger(apiLogger), api.WithEnabledBackendLogger(backendLogging))
		return err
	},

	SilenceErrors: true,
	SilenceUsage:  true,
}

// Execute: initializes and runs the root command
func Execute() {
	if err := RootCmd.Execute(); err != nil {
		message := fmt.Sprintf("Error during command execution: %s.", err)
		fmt.Println(message)
	}
}
