package actions

import (
	"fmt"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/pcanilho/rocket.chat-cli/api"
	"gitlab.com/pcanilho/rocket.chat-cli/helpers/formatters"
)

const templateId = "channel_history"

var tableHeader = []string{"channel", "user", "message"}
var extendedTableHeader = []string{"username", "timestamp", "mentions"}

var historyCmd = &cobra.Command{
	Use:   "history <channel|group> <channel|group> ...",
	Short: "Fetches the message history for the provided channels and/or groups names",
	RunE: func(cmd *cobra.Command, channels []string) error {
		// Flags
		formatterMode, _ := cmd.Flags().GetString("mode")
		expandedView, _ := cmd.Flags().GetBool("expand")
		templateGlob, _ := cmd.Flags().GetString("template-glob")
		outputMode, _ := cmd.Flags().GetString("output")
		outputFile, _ := cmd.Flags().GetString("file")


		// Connect to the API
		rocketClient, err := api.GetInitialisedInstance()
		if err != nil {
			return err
		}
		if err := rocketClient.Connect(); err != nil {
			return err
		}

		// Formatter choice
		formatter, err := GetFormatter(formatterMode, templateGlob)
		if err != nil {
			return err
		}

		for _, channel := range channels {
			// Get all channelMessages
			foundChannels, err := rocketClient.GetMessages(channel, 0)
			if err != nil {
				return err
			}

			// Go through all the found channels (inc. fuzzy-search & all filters)
			for _, channelMessages := range foundChannels {
				// Message line
				for _, message := range channelMessages.Messages {
					var userMentions []string
					for _, mention := range message.Mentions {
						userMentions = append(userMentions, mention.Name)
					}

					line := []string{
						channel,
						message.User.Name,
						strings.TrimSpace(message.Msg),
					}
					if expandedView {
						line = append(line,
							message.User.UserName,
							message.Timestamp.String(),
							strings.Join(userMentions, ","))
					}
					// Append line to the formatter stream
					data := formatters.GetFormatterDataWithData(line)
					if err := formatter.Append(data); err != nil {
						return err
					}
				}

				// Table formatter
				outputHeader := tableHeader
				if expandedView {
					outputHeader = append(outputHeader, extendedTableHeader...)
				}

				// Compose formatter data
				data := formatters.GetFormatterData()
				data.Headers = outputHeader
				data.Data = channelMessages


				// Writer choice
				var outputFileName string
				if outputFileName = strings.TrimSpace(outputFile); len(outputFile) == 0 {
					outputFileName = fmt.Sprintf("%s.md", channelMessages.ChannelName)
				}

				writer, err := GetWriter(outputMode, outputFileName)
				if err != nil {
					return err
				}

				if err := formatter.Print(templateId, data, writer); err != nil {
					return err
				}
			}
		}
		return nil
	},
}

var messagesCmd = &cobra.Command{
	Use:        "messages <action>",
	Short: 		"Fetches rocket chat message",
	Example:    "rocket messages history <channel|group>",
	Aliases:    []string{"m", "message", "msg"},
	SuggestFor: []string{"m", "message", "msg"},
}

func init() {
	historyCmd.Flags().BoolP("expand", "e", false,
		"If specified, extended information will be printed for the action")
	historyCmd.Flags().StringP("mode", "m", "table",
		"Selects which output mode should be used to print the information [table|template]")
	historyCmd.Flags().String("template-glob", "templates/*.gohtml",
		"The file pattern that should be used to load Go templates. These templates will then be used for"+
			" output formatting")
	historyCmd.Flags().StringP("output", "o", "stdout",
		"Where the output of this command should be sent [stdout|file]")
	historyCmd.Flags().StringP("file", "f", "",
		"Destination file if the [file] output mode was selected. If left empty, the channel name will be used.")

	messagesCmd.AddCommand(historyCmd)
	RootCmd.AddCommand(messagesCmd)
}
