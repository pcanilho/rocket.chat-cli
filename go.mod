module gitlab.com/pcanilho/rocket.chat-cli

go 1.15

require (
	github.com/olekukonko/tablewriter v0.0.4
	github.com/pkg/errors v0.9.1
	github.com/spf13/cobra v1.0.0
	gitlab.com/pcanilho/rocket.chat.go.sdk v0.1.0
)
