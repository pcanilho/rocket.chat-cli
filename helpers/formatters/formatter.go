package formatters

import (
	"io"
	"sync"
)

type Formatter interface {
	Print(string, *FormatterData, io.Writer) error
	PrintStream(io.Writer) error
	Append(*FormatterData) error
}

type FormatterData struct {
	Headers interface{}
	Data    interface{}
}

var FormatterDataPool *sync.Pool

func GetFormatterData() *FormatterData {
	return FormatterDataPool.Get().(*FormatterData)
}

func GetFormatterDataWithHeaders(headers interface{}) *FormatterData {
	fmtData := GetFormatterData()
	fmtData.Headers = headers
	return fmtData
}

func GetFormatterDataWithData(data interface{}) *FormatterData {
	fmtData := GetFormatterData()
	fmtData.Data = data
	return fmtData
}

func init() {
	FormatterDataPool = &sync.Pool{
		New: func() interface{} { return new(FormatterData) },
	}
}
