package formatters

import (
	"fmt"
	"io"
	"text/template"

	"github.com/pkg/errors"

	"github.com/olekukonko/tablewriter"
)

type tableFormatter struct {
	table    *tablewriter.Table
	template *template.Template
	rows     map[uint16][]string
	rowCount uint16
}

func NewTablePrinter(templatesGlob string) (*tableFormatter, error) {
	tmpl, err := template.New("tb_container").ParseGlob(templatesGlob)
	if err != nil {
		return nil, errors.Wrapf(err, "unable to load templates from the supplied glob [%v]", templatesGlob)
	}
	return &tableFormatter{template: tmpl, rows: make(map[uint16][]string)}, nil
}

func (t *tableFormatter) Append(data *FormatterData) error {
	if line, ok := data.Data.([]string); !ok {
		return fmt.Errorf("expected the type []string but got [%T] instead", data)
	} else {
		t.rows[t.rowCount] = line
		t.rowCount++
		return nil
	}
}

func (t *tableFormatter) Print(templateId string, data *FormatterData, writer io.Writer) error {
	tableHeader, ok := data.Headers.([]string)
	if !ok {
		return fmt.Errorf("the provided data must be of type []string")
	}
	if t.table == nil {
		t.table = tablewriter.NewWriter(writer)
	}
	for _, line := range t.rows {
		t.table.Append(line)
	}
	t.table.SetAutoWrapText(false)
	t.table.SetAlignment(tablewriter.ALIGN_LEFT)
	t.table.SetHeader(tableHeader)
	t.table.Render()
	return nil
}

func (t *tableFormatter) PrintStream(_ io.Writer) error {
	return fmt.Errorf("not implemented")
}
