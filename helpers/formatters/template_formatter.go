package formatters

import (
	"bytes"
	"fmt"
	"io"
	"text/template"

	"github.com/pkg/errors"
)

type templateFormatter struct {
	template *template.Template
	stream   bytes.Buffer
}

func NewTemplateFormatter(templatesGlob string) (*templateFormatter, error) {
	tmpl, err := template.ParseGlob(templatesGlob)
	if err != nil {
		return nil, errors.Wrapf(err, "unable to load templates from the supplied glob [%v]", templatesGlob)
	}
	return &templateFormatter{template: tmpl}, nil
}

func (mdf *templateFormatter) Print(templateId string, data *FormatterData, writer io.Writer) error {
	return errors.Wrap(mdf.template.ExecuteTemplate(writer, templateId, data.Data), "unable to render the markdown template")
}

func (mdf *templateFormatter) PrintStream(writer io.Writer) error {
	_, err := writer.Write(mdf.stream.Bytes())
	return errors.Wrap(err, "unable to print the stream of data")
}

func (mdf *templateFormatter) Append(data *FormatterData) error {
	mdf.stream.WriteString(fmt.Sprintf("%v", data.Data))
	return nil
}
