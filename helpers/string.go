package helpers

import "strings"

func EmptyString(value string) bool {
	return len(strings.TrimSpace(value)) == 0
}
