package api

import (
	"fmt"
	"github.com/pkg/errors"
	"gitlab.com/pcanilho/rocket.chat-cli/models/extended"
	"gitlab.com/pcanilho/rocket.chat.go.sdk/models"
	rocketApi "gitlab.com/pcanilho/rocket.chat.go.sdk/rest"
	"gitlab.com/pcanilho/rocket.chat-cli/helpers"
	"io/ioutil"
	"log"
	"net/url"
	"regexp"
	"strings"
	"time"
)

// Constants
const apiTimeout = 14 * time.Second

var _instance *rocketChatClient
type rocketChatClient struct {
	serverUrl 	string
	client 		*rocketApi.Client
	credentials *Credentials

	apiLogger *log.Logger
	backendLogging bool
}

func GetInstance(serverUrl string, credentials *Credentials, opts ...RocketChatOption) (*rocketChatClient, error) {
	if _instance != nil { return _instance, nil }
	if helpers.EmptyString(credentials.TokenID) || helpers.EmptyString(credentials.Token) {
		return nil, fmt.Errorf("the provided [token Id] & [token] cannot be empty or whitespaced")
	}
	_instance = &rocketChatClient{}
	_instance.credentials = credentials
	_instance.serverUrl = serverUrl

	// Instance opts
	for _, opt := range opts { opt(_instance) }
	_instance.setDefaults()

	// URL
	parsedUrl, err := url.Parse(serverUrl)
	if err != nil {
		return nil, errors.Wrapf(err, "unable to parse the value [%v] as a valid URL", serverUrl)
	}

	// SDK
	_instance.client = rocketApi.NewClient(parsedUrl, _instance.backendLogging)
	return _instance, nil
}

func GetInitialisedInstance() (*rocketChatClient, error) {
	if _instance == nil {
		return nil, fmt.Errorf("a [rocketChatClient] instance needs to be initialised, please use [GetInstance]")
	}
	return _instance, nil
}

func WithApiLogger(logger *log.Logger) RocketChatOption {
	return func(chat *rocketChatClient) {
		chat.apiLogger = logger
	}
}

func WithEnabledBackendLogger(value bool) RocketChatOption {
	return func(chat *rocketChatClient) {
		chat.backendLogging = value
	}
}

type Credentials struct {
	TokenID, Token string
}

type RocketChatOption = func(*rocketChatClient)

func (rc *rocketChatClient) Connect() error {
	return errors.Wrapf(rc.client.Login(&models.UserCredentials{
		ID: 	rc.credentials.TokenID,
		Token:  rc.credentials.Token,
	}), "unable to login with the provided credentials to the Rocket.Chat server [%v]", rc.serverUrl)
}

func (rc *rocketChatClient) Disconnect() (string, error) {
	if rc.client == nil { return "not connected", nil }
	serverMessage, err := rc.client.Logout()
	return serverMessage, errors.Wrapf(err, "unable to disconnect from the Rocket.Chat server [%v]", rc.serverUrl)
}

// GetMessages : Get all the messages from the specified channelId
// :param channelId the id of the channel from which to get messages
// :param limit the amount of messages to return. If left to 0, all messages will be fetched
func (rc *rocketChatClient) GetMessages(channelName string, limit int) (out []*extended.ChannelMessagesExtended, err error) {
	if rc.client == nil { return nil, fmt.Errorf("the [rocketChatClient] instance is not connected") }

	channels, err := rc.findChannelByName(channelName)
	if err != nil { return nil, err	}
	rc.apiLogger.Printf("Successfully found [%v] matches for the channel name [%v]", len(channels), channelName)
	for i, extChannel := range channels {
		channel := &models.Channel{ID: extChannel.Id}
		pagination := &models.Pagination{Count: limit}
		messages, err := rc.client.GetMessages(channel, &extChannel.Type, pagination)
		if err != nil {
			return nil, err
		}

		rc.apiLogger.Printf("Downloaded [%v] messages from the channel name [%v]", len(messages), extChannel.Name)
		out = append(out, &extended.ChannelMessagesExtended{Messages: messages, ChannelName: extChannel.Name})
		if i < len(channels) - 1{
			rc.apiLogger.Printf("Sleeping to avoid the API eviction period for [%v]", apiTimeout.String())
			<- time.After(apiTimeout)
		}
	}

	if len(out) == 0 {
		return nil, errors.Wrapf(err, "unable to get messages from the Rocket.Chat server [%v]", rc.serverUrl)
	}

	return
}

func (rc *rocketChatClient) setDefaults() *rocketChatClient {
	if rc.apiLogger == nil {
		rc.apiLogger = log.New(ioutil.Discard, "", 0)
	}
	return rc
}

func (rc *rocketChatClient) findChannelByName(channelName string) (out []extended.ChannelExtended, err error) {
	all := strings.ToLower(channelName) == "all"
	regex, err := regexp.Compile(channelName)
	if err != nil {
		return nil, errors.Wrapf(err, "[%v] is not a valid regex expression", channelName)
	}

	// Groups
	joinedGroups, err := rc.client.GetJoinedGroups()
	if err != nil {
		return nil, errors.Wrapf(err, "unable to list the user's joined groups")
	}

	// Append found joined groups
	for _, group := range joinedGroups.Groups {
		if all || regex.MatchString(group.Name) {
			out = append(out, extended.ChannelExtended{
				Id:   group.ID,
				Name: group.Name,
				Type: models.MessageFromGroup,
			})
		}
	}

	// Joined channels
	joinedChannels, err := rc.client.GetJoinedChannels(nil)
	if err != nil {
		return nil, errors.Wrapf(err, "unable to list the user's joined channels")
	}

	// Append found joined channels
	for _, joinedChannel := range joinedChannels.Channels {
		if all || regex.MatchString(joinedChannel.Name) {
			out = append(out, extended.ChannelExtended{
				Id:   joinedChannel.ID,
				Name: joinedChannel.Name,
				Type: models.MessageFromChannel,
			})
		}
	}

	// Public channels
	publicChannels, err := rc.client.GetPublicChannels()
	if err != nil {
		return nil, errors.Wrapf(err, "unable to list the public channels")
	}

	// Append found public channels
	for _, publicChannel := range publicChannels.Channels {
		if all || regex.MatchString(publicChannel.Name) {
			out = append(out, extended.ChannelExtended{
				Id:   publicChannel.ID,
				Name: publicChannel.Name,
				Type: models.MessageFromChannel,
			})
		}
	}

	if len(out) == 0 {
		return nil, fmt.Errorf("unable to find the channel name [%v] in joined & public channels/groups list", channelName)
	}

	return
}
