# Target configuration
TARGET_OS		:= linux
TARGET_ARCH		:= amd64
CGO_ENABLED		:= 0

RELEASE			:= 0.1.0
SHA				:= $(shell git rev-list -1 HEAD)
DATE            := $(shell git log -1 --date=short --pretty=format:%cd)
APP_NAME		:= rocket-cli
VPATH			:= gitlab.com/pcanilho/rocket.chat-cli/actions

OUTPUT_DIR		:= output

ifeq ($(TARGET_OS),windows)
	APP_EXTENSION = .exe
endif

build:
	CGO_ENABLED=$(CGO_ENABLED) GOOS=$(TARGET_OS) GOARCH=$(TARGET_ARCH) \
	go build -trimpath \
		-ldflags "-X $(VPATH).name=$(APP_NAME) -X $(VPATH).version=$(RELEASE) -X $(VPATH).commit=$(SHA)" \
		-o $(OUTPUT_DIR)/$(APP_NAME).v$(RELEASE)_$(TARGET_OS)_$(TARGET_ARCH)$(APP_EXTENSION) -- cmd/rocket.go

all:
	for os in windows linux darwin; \
	do \
	 for arch in amd64; \
	 	do \
	 	make build TARGET_OS=$$os TARGET_ARCH=$$arch;\
	 done; \
	done;
