package extended

import "gitlab.com/pcanilho/rocket.chat.go.sdk/models"

type ChannelExtended struct {
	Id, Name string
	Type     models.MessageChannelType
}
