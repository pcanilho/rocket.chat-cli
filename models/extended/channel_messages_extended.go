package extended

import (
	"gitlab.com/pcanilho/rocket.chat.go.sdk/models"
)

type ChannelMessagesExtended struct {
	Messages    []models.Message
	ChannelName string
}

func (cme *ChannelMessagesExtended) GetGroupedMessages() (out map[DateEntry][]models.Message) {
	if cme.Messages == nil {
		return
	}
	var dateEntry DateEntry
	for _, message := range cme.Messages {
		dateEntry.YearDay = message.Timestamp.YearDay()
		dateEntry.Month = message.Timestamp.Month()
		dateEntry.Year = message.Timestamp.Year()

		out[dateEntry] = append(out[dateEntry], message)
	}
	return
}
