package extended

import "time"

type DateType = int

type DateEntry struct {
	YearDay int
	Month   time.Month
	Year    int
}
