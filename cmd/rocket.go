package main

import "gitlab.com/pcanilho/rocket.chat-cli/actions"

func main() {
	actions.Execute()
}
