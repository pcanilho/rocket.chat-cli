# `rocket.chat-cli`

The `rocket.chat-cli` aims to provide a command line interface to interact with a given `Rocket.Chat` server. It makes use
of a custom forked version [rocket.chat.go.sdk](https://gitlab.com/pcanilho/rocket.chat.go.sdk) of the original found 
[here](https://github.com/RocketChat/Rocket.Chat.Go.SDK).

### Building

For convenience, a `Makefile` has been included that facilitates the building of this project:

```bash
make build
```
### Example usage

##### > Fetch the message `history` for a given list of channels and/or grou[s]

```bash
rocket messages history <channel1> <channel2> <group1> -s <rocket.chat.server> -i <token.id> -t <token>
```

##### > Alternatively, the server address, token ID and token can be supplied via environment variables

```bash
export ROCKET_SERVER=<rocket.chat.server>
export ROCKET_TOKEN_ID=<token.id>
export ROCKET_TOKEN=<token>

rocket message history <channel1>
```

##### > The channel & groups discovery supports `regex` patterns

```bash
# Get all channels that start with the prefix:  channel_.*
# Get all groups that end with the suffix:      _chat$
rocket messages history channel_.* _chat$ -s <rocket.chat.server> -i <token.id> -t <token>
```

##### > Dump the contents in a file with the same name of the channel/group 

```bash
rocket messages history <channel1> <channel2> <group1> -s <rocket.chat.server> -i <token.id> -t <token> -o file
```

##### > Format the dump with the supplied templates

```bash
rocket messages history <channel1> <channel2> <group1> -s <rocket.chat.server> -i <token.id> -t <token> -o file -m template
```

### Available actions

```bash
Usage:
  rocket messages [command]

Aliases:
  messages, m, message, msg

Examples:
messages history <channel|group>

Available Commands:
  history     Fetches the message history for the provided channels and/or groups names

Flags:
  -h, --help   help for messages

Global Flags:
  -i, --id string       The token Id to be used when contacting a Rocket.Chat server. [ROCKET_TOKEN_ID]
  -s, --server string   The server address to be used when contacting a Rocket.Chat server.
  -t, --token string    The token to be used when contacting a Rocket.Chat server. If not provided, the user will be prompted for each action. [ROCKET_TOKEN]
  -v, --verbose count   If provided, debug information will be printed to the stdout.

Use "rocket messages [command] --help" for more information about a command.

```

### History action
```bash
Fetches the message history for the provided channels and/or groups names

Usage:
  rocket messages history <channel|group> <channel|group> ... [flags]

Flags:
  -e, --expand                 If specified, extended information will be printed for the action
  -f, --file string            Destination file if the [file] output mode was selected. If left empty, the channel name will be used.
  -h, --help                   help for history
  -m, --mode string            Selects which output mode should be used to print the information [table|template] (default "table")
  -o, --output string          Where the output of this command should be sent [stdout|file] (default "stdout")
      --template-glob string   The file pattern that should be used to load Go templates. These templates will then be used for output formatting (default "templates/*.gohtml")

Global Flags:
  -i, --id string       The token Id to be used when contacting a Rocket.Chat server. [ROCKET_TOKEN_ID]
  -s, --server string   The server address to be used when contacting a Rocket.Chat server.
  -t, --token string    The token to be used when contacting a Rocket.Chat server. If not provided, the user will be prompted for each action. [ROCKET_TOKEN]
  -v, --verbose count   If provided, debug information will be printed to the stdout.
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
[GPL-3.0](https://choosealicense.com/licenses/gpl-3.0/)
